import { Router } from "express";
import loginMiddleware from "../../shared/http/middlewares/loginMiddleware";
import loginController from "../controllers/loginController";
import PhotoController from "../controllers/PhotoController";

const PhotosRouters = Router();

PhotosRouters.post("/login", loginController.login); 
PhotosRouters.post("/places",loginMiddleware, PhotoController.savePlace); 
PhotosRouters.get("/places",loginMiddleware, PhotoController.listPhotos); 
PhotosRouters.get("/places/:placeId",loginMiddleware, PhotoController.listOnePhotos); 
PhotosRouters.put("/places",loginMiddleware, PhotoController.UpdatePhotos); 
PhotosRouters.delete("/places/:placeId",loginMiddleware, PhotoController.DeletePhotos); 

export default PhotosRouters;
