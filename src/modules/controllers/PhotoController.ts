import { Request, Response } from "express";
import bcrypt from "bcrypt";
import SavePhotoService from "../services/photoServices/SavePhotoService";
import ListPhotoService from "../services/photoServices/ListPhotoService";
import ListOnePhotoService from "../services/photoServices/ListOnePhotoService";
import UpdatePhotoService from "../services/photoServices/UpdatePhotoService";
import DeletePhotoService from "../services/photoServices/DeletePhotoService";

class PhotosController {
  async savePlace(req: Request, res: Response) {
    const { name } = req.body;
    const saved = await SavePhotoService.service(name);
    return res.json(saved);
  }
  async listPhotos(req: Request, res: Response) {
    const listed = await ListPhotoService.service(req.body);
    return res.json(listed);
  }
  async listOnePhotos(req: Request, res: Response) {
    const {placeId} = req.params
    const listed = await ListOnePhotoService.service(placeId);
    return res.json(listed);
  }
  async UpdatePhotos(req: Request, res: Response) {
    const listed = await UpdatePhotoService.service(req.body);
    return res.json(listed);
  }
  async DeletePhotos(req: Request, res: Response) {
    const {placeId} = req.params
    const listed = await DeletePhotoService.service(placeId);
    return res.json(listed);
  }
}

export default new PhotosController();
