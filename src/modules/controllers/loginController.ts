import { Request, Response } from "express";
import { UserModel } from "../models/UserModel";
import LoginService from "../services/loginServices/LoginService";
import bcrypt from 'bcrypt'


class loginController{
    async login(req:Request,res:Response){
        const loginUser = await LoginService.service(req.body)
        return res.json(loginUser)
    }
}

export default new loginController()