import { loginI } from "../../../interfaces/LoginInterface";
import AppError from "../../../shared/errors/AppError";
import { UserModel } from "../../models/UserModel";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

class LoginService {
  async service(login: loginI) {
    const findEmail = await UserModel.find({ email: login.email }).exec();
    if (!findEmail[0]) {
      throw new AppError("invalid Email", 400);
    }
    const compared = await bcrypt.compare(
      login.password,
      findEmail[0].password
    );
    if (!compared) {
      throw new AppError("invalid password", 400);
    }
    const token = await jwt.sign(
      { id: findEmail[0]._id },
      `${process.env.SECRET}`,
      {
        expiresIn: "1d",
      }
    );
    return {token, user:{
        _id: findEmail[0]._id,
        name: findEmail[0].name
    }};
  }
}
export default new LoginService();
