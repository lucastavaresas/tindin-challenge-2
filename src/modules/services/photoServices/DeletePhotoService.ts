import AppError from "../../../shared/errors/AppError";
import { PhotoModel } from "../../models/PhotoModel";
require("dotenv").config();

class DeletePhotoService {
  async service(placeId: string) {
    const find = await PhotoModel.findById(placeId);
    if (!find) {
      throw new AppError("Photo not found", 404);
    }
    await PhotoModel.findByIdAndDelete(placeId);
    return {
      _id: find._id,
      name: find.name,
      photo: find.photo
    }
  }
}
export default new DeletePhotoService();
