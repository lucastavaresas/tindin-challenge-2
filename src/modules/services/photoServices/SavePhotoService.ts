import { Unsplash } from "../../config/AxiosConfig";
import { PhotoModel } from "../../models/PhotoModel";
require("dotenv").config();

class SavePhotoService {
  async service(name: string) {
    let named = name.normalize("NFD").replace(/[^a-zA-Zs]/g, "").toLowerCase()
    const foundPhoto = await Unsplash.get(
      `https://api.unsplash.com/search/photos?page=1&query=${name}&client_id=${process.env.UNSPLASH_KEY}`
    ).then(async (res) => {
        for(let result of res.data.results){
            const findRepeated = await PhotoModel.findOne({photo: result.links.download})
            if(!findRepeated){
               return result.links.download
            }
            continue
        }
    });
    const createPhoto = await new PhotoModel({
      name: named,
      photo: foundPhoto,
    });
    await createPhoto.save();

    return {
      _id: createPhoto._id,
      name: name,
      photo: createPhoto.photo,
    };
  }
}
export default new SavePhotoService();
