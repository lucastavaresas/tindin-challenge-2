import AppError from "../../../shared/errors/AppError";
import { ListI } from "../../../interfaces/ListInterface";
import { PhotoModel } from "../../models/PhotoModel";
require("dotenv").config();

class ListPhotoService {
  async service(list: ListI) {
    const skip = list.limit * (list.page - 1);
    const listed = await PhotoModel.find({ name: { $regex: `${list.search}` } })
      .skip(skip)
      .limit(list.limit)
      .sort([[`${list.order}`, -1]]);
    if (!listed[0]) {
      throw new AppError("nothing was found", 404);
    }
    return listed;
  }
}
export default new ListPhotoService();
