import AppError from "../../../shared/errors/AppError";
import { PhotoModel } from "../../models/PhotoModel";
import { UpdateI } from "../../../interfaces/UpdateInterface";
import { Unsplash } from "../../config/AxiosConfig";
require("dotenv").config();

class UpdatePhotoService {
  async service(update: UpdateI) {
    const find = await PhotoModel.find({_id: update._id}).catch(()=>{
      throw new AppError("photo not found", 404);
    })
    const found = find[0]
    if (found.name == update.name) {
      throw new AppError("Must insert a new name", 400);
    }
    const foundPhoto = await Unsplash.get(
      `https://api.unsplash.com/search/photos?page=1&query=${update.name}&client_id=${process.env.UNSPLASH_KEY}`
    ).then(async (res) => {
      for (let result of res.data.results) {
        const findRepeated = await PhotoModel.findOne({
          photo: result.links.download,
        });
        if (!findRepeated) {
          return result.links.download;
        }
        continue;
      }
    });
    found.name = update.name.normalize("NFD").replace(/[^a-zA-Zs]/g, "").toLowerCase()
    found.photo = foundPhoto
    await found.save()
    return {
      _id:  found._id,
      name: update.name,
      photo:  found.photo
    }
  }
}
export default new UpdatePhotoService();
