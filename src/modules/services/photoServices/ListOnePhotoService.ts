import AppError from "../../../shared/errors/AppError";
import { PhotoModel } from "../../models/PhotoModel";
require("dotenv").config();

class ListOnePhotoService {
  async service(placeId: string) {
    const listed = await PhotoModel.findById(placeId);
    if (!listed) {
      throw new AppError("photo not found", 404);
    }
    return listed;
  }
}
export default new ListOnePhotoService();
