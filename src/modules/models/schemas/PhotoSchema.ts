import { Schema } from "mongoose";

export const PhotoSchema = new Schema(
  {
    name: { type: String, required: true },
    photo: { type: String, required: true },
  }
);
