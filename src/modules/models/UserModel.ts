import { model } from "mongoose";
import { UserSchema } from "./schemas/UserSchema";


export const UserModel = model("User", UserSchema);
