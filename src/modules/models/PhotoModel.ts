import { model } from "mongoose";
import { PhotoSchema } from "./schemas/PhotoSchema";


export const PhotoModel = model("Photo", PhotoSchema);
