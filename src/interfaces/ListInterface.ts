export interface ListI {
  search: string;
  order: string;
  page: number;
  limit: number;
}
