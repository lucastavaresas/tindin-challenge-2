import { connect } from 'mongoose'
require('dotenv').config()

const urlMongo = `${process.env.MONGO_HTTP_LINK}`

connect(urlMongo!)
  .then(() => {
    console.log('MongoDb is running')
  })
  .catch(err => {
    console.log(err)
  })
