import "express-async-errors";
import "reflect-metadata";
import cors from "cors";
import express, { NextFunction, Request, Response } from "express";
import "../repositories/MongoConnection";
import AppError from "../errors/AppError";
import PhotosRouters from "../../modules/routes/PhotosRouters";
require("dotenv").config();

const app = express();
const PORT = process.env.PORT || 5050;

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(PhotosRouters)

app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
  if (error instanceof AppError) {
    return res.status(error.statusCode).json({
      status: error.statusCode,
      message: error.message,
    });
  }
  return res.status(500).json({
    message: "Internal server error",
    status: 500,
  });
});

if (process.env.NODE_ENV !== "test") {
  app.listen(PORT,()=>{
    console.log('its running on 5050')
  });
}

export { app };

//set NODE_ENV=test
