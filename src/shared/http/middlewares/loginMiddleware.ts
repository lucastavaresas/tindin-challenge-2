
import { NextFunction, Request, Response } from 'express'
import jwt from 'jsonwebtoken'
import AppError from '../../errors/AppError'
require('dotenv').config()

interface TokenPayLoad {
  _id: string
  iat: number
  exp: number
}

export default function (
  req: Request,
  res: Response,
  next: NextFunction,
) {
  const { authorization } = req.headers
  if (!authorization) {
    throw new AppError('anauthorized', 401)
  }
  const token = authorization.replace('Bearer', '').trim()
  try {
    const data = jwt.verify(token, `${process.env.SECRET}`)
    const { _id } = data as TokenPayLoad
    req.userId = _id
    next()
  } catch {
    throw new AppError('anauthorized', 401)
  }
}
