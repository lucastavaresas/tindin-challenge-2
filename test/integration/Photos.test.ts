import request from "supertest";
import { PhotoModel } from "../../src/modules/models/PhotoModel";
import { UserModel } from "../../src/modules/models/UserModel";
import SavePhotoService from "../../src/modules/services/photoServices/SavePhotoService";
import { app } from "../../src/shared/http/index";
import jwt from "jsonwebtoken";
import UpdatePhotoService from "../../src/modules/services/photoServices/UpdatePhotoService";
import { UpdateI } from "../../src/interfaces/UpdateInterface";
import AppError from "../../src/shared/errors/AppError";
require("dotenv").config();

jest.mock("../../src/modules/services/photoServices/SavePhotoService", () => ({
  service: jest.fn(async (name) => {
    const createPhoto = await new PhotoModel({
      name: name,
      photo: "found photo",
    });
    return {
      _id: createPhoto._id,
      name: name,
      photo: createPhoto.photo,
    };
  }),
}));

jest.mock(
  "../../src/modules/services/photoServices/UpdatePhotoService",
  () => ({
    service: jest.fn(async (update: UpdateI) => {
      const find = await PhotoModel.find({_id: update._id}).catch(()=>{
      throw new AppError("photo not found", 404);
    })
    const found = find[0]
    if (found.name == update.name) {
      throw new AppError("Must insert a new name", 400);
    }
    return {
      _id:  found._id,
      name: update.name,
      photo:  "any-photo"
    }
    }),
  })
);

describe("Photos", () => {
  it("Should return the saved photo", async () => {
    const mocked = await SavePhotoService.service("lucas");
    expect(mocked).toBeDefined();
    expect(mocked).toBeInstanceOf(Object);
    expect(mocked.name).toBe("lucas");
  });
  it("Should list all saved photo", async () => {
    const found = await UserModel.findOne({});
    const token = await jwt.sign({ id: found._id }, `${process.env.SECRET}`, {
      expiresIn: "1d",
    });
    const response = await request(app)
      .get(`/places`)
      .set("Authorization", `Bearer ${token}`)
      .send({
        search: "o",
        order: "name",
        page: 1,
        limit: 2,
      });
    expect(response.statusCode).toBe(200);
  });
  it("Should list one saved photo", async () => {
    const found = await UserModel.findOne({});
    const token = await jwt.sign({ id: found._id }, `${process.env.SECRET}`, {
      expiresIn: "1d",
    });
    const anyPhoto = await PhotoModel.findOne({});
    const response = await request(app)
      .get(`/places/${anyPhoto._id}`)
      .set("Authorization", `Bearer ${token}`);
    expect(response.statusCode).toBe(200);
  });
  it("Should Update one saved photo", async () => {
    const foundAny = await PhotoModel.findOne({})
    const mocked = await UpdatePhotoService.service({_id: `${foundAny._id}`,name: "test"});
    expect(mocked).toBeDefined();
    expect(mocked).toBeInstanceOf(Object);
    expect(mocked.name).toBe("test");
  });
  it("Should delete saved photo", async () => {
    const found = await UserModel.findOne({});
    const token = await jwt.sign({ id: found._id }, `${process.env.SECRET}`, {
      expiresIn: "1d",
    });
    const anyPhoto = await PhotoModel.findOne({});
    const response = await request(app)
      .delete(`/places/${anyPhoto._id}`)
      .set("Authorization", `Bearer ${token}`);
    expect(response.statusCode).toBe(200);
  });
});
