import { hash } from "bcrypt";
import request from "supertest";
import { UserModel } from "../../src/modules/models/UserModel";
import { app } from "../../src/shared/http/index";

describe("login", () => {
    it('should generate a token',async ()=>{
        const found =  new UserModel({
            name: "name",
            email: "lucas@silva.com",
            password: await hash("12345",10)
        })
        await found.save()
        const response = await request(app)
        .post(`/login`)
        .send({
          email: found.email,
          password: found.password
        }
        );
      expect(response.statusCode).toBe(200);  
    })
  });

